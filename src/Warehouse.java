package tema3var2;

/**
 * Clasa implementeaza un depozit careare in spate un Arbore Binar de Cautare
 * Nodurile acestuia sunt instante ale clasei Produs
 * @author Sorana
 *
 */
public class Warehouse 
{
	/** Radacina arborelui */
    public Product root;
	
	public Warehouse()
	{ root=null; }
	
	
	
	/**
	 * Inseararea unui nou produs in arbore  , sau incrementarea catitatii si 
     * actualizarea la ulimul pret mentionat
	 * @param cod =codul noului produs
	 * @param pr =pretul nou
	 * @param cantit =cantitatea
	 */
    public void insereaza(String denum,float pr,int cantit,String a) 
    {
      Product prod=new Product(denum,pr,cantit,a);	
       root = insert( prod, root );
    }
    
   
    
    /**
     * Metoda interioara de inserare in arbore
     * Inserarea se face dupa codul fiecarui produs
     * @param x Produsul nou care se insereaza
     * @param t radacina arborelui
     * @return noua radacina
     */
    protected Product insert( Product x, Product t )
    {
        if( t == null )
            t = x;
        else if( x.denumire.hashCode() < t.denumire.hashCode()  )
            t.left = insert( x, t.left );
        else if(x.denumire.hashCode() > t.denumire.hashCode()  )
            t.right = insert( x, t.right );
        else
        	 //produsul cu cadul cautat se gaseste deja in stoc 
        {
        	t.incremNr(x.getNr());
        	t.setPretN(x.getPretN());        	
        }
        return t;
    }
    
    /**
     * Metoda de modificare a pretului unui produs
     * @param a -prosului
     * @param b - nou pret
     * @return
     */
    public boolean modifPret(String a,float b)
    {
    	Product x;
    	x=find(a,root);
    	
    	if(x!=null)
    	   { x.setPretN(b);
    	   return true;
    	   }
    	else return false;
    }
    
    /**
     * Metoda de modificare a cantitatii unui produs
     * @param a -prosului
     * @param b - noua cantitate
     * @return
     */
    public boolean modifCantit(String a,int c)
    {
    	Product x;
    	x=find(a,root);
    	
    	if(x!=null)
    	   { 
    		if(c>0)
    		x.setNr(c);
    		else this.sterge(a);
    	   return true;
    	   }
    	else return false;
    }
    
    /**
     * Cautarea unui anumit cod in arbore
     * @param cod
     * @return string cu informatii
     */
    public String cauta(String q)
    {
    	Product x;
    	String s=new String("");
    	x=find(q,root);
    	
    	if(x!=null)
    	  s="Produs "+q+" : "+x.getNr()+" bucati, pret "+x.getPretN()+"$";
    	else s="Produsul "+q+" nu a fost gasit";
    	
    	return s;
    }
    
    /**
     * Sterge din arbore produsul cu codul mentionat
     * Pentru a putea sterge trebuie ca prima data sa gasim produsul cu respectivul cod
     * @param cod codul de inlaturat
     * @return true daca codul a existat si a putut fi sters ,false dca codul nu exista in arbore
     */
    public boolean sterge( String q )
    {
    	Product x;
    	x=find(q,root);
    	if(x!=null)
          { root = remove( q, root );
            return true;
          }
    	else return false;
    }
    
    /**
     * Metoda interioara de sters dintr-un subarbore
     * Deoarece nu se intra in acesta metoda pentru t-null , atunci putem sa sarim peste acea testare
     * @param cod =codul de sters 
     * @param t radacina subarborelui
     * @return noua radacina
     */
    protected Product remove( String s, Product t )
    {
    	Product x;
        if( s.hashCode() < t.denumire.hashCode())
            t.left = remove( s, t.left );
        else if( s.hashCode() > t.denumire.hashCode() )
            t.right = remove( s, t.right );
        else 
        	if( t.left != null && t.right != null ) // Two children
            {
        		x=gasesteMin( t.right );
               t.denumire =x .denumire;
               t.setNr(x.getNr()); //mutam informatiile din ultimul nod aici
               t.setPretN(x.getPretN());
               t.setCompanie(x.getCompanie());
               t.right = stergeMin( t.right );
            } 
        	else
               t = ( t.left != null ) ? t.left : t.right;
        return t;
    }
    
    /**
     * Gasirea celui mai mic cod din subarbore
     * @param t radacina subarborelui
     * @return nodul cu cel mai mic cod
     */
    protected Product gasesteMin( Product t) 
    {
        if( t != null )
            while( t.left != null )
                t = t.left;
        
        return t;
    }
    
    /**
     * Stergerea minimului dintr=un subarbore
     * @param t radacina sunarb
     * @return noua rad
     */
    protected Product stergeMin( Product t ) 
    {
        if( t.left != null )
        {
            t.left = stergeMin( t.left );
            return t;
        } else
            return t.right;
    }
    
    
    /**
     * Metoda  de gasire a produsului dupa cod
     * @param cod =codul cautat
     * @param t radacina arborelui
     * @return noduc cu codul cautat
     */
    public Product find( String q , Product t ) 
    {
        while( t != null ) 
        {
            if( q.hashCode() < t.denumire.hashCode() )
                t = t.left;
            else 
            	if( q.hashCode() > t.denumire.hashCode() )
                t = t.right;
            else
                return t;    // gasit
        }
        
        return null;         // nu l-am gasit
    }
    
    /**
     * Arborele va fi logic gol
     */
    public void goleste( ) 
    {
        root = null;
    }
    
    /**
     * Testeaza daca arborele este gol
     * @return true daca e gol, false altfel
     */
    public boolean isEmpty( ) 
    {
        return root == null;
    }
    
    /**
     * Metoda de marire a pretului tuturor produselor
     * @param a procentul de modificare
     * @param t -produsul caruia i se modifica pretul
     */
    public void marirePret(int a,Product t)
    {
    	if(t!=null)
    	{
    		t.marestePret(a);
    		marirePret(a,t.left);
    		marirePret(a,t.right);
    	}
    }
    
    /**
     * Metoda de micsorare a pretului tuturor produselor
     * @param a procentul de modificare
     * @param t -produsul caruia i se modifica pretul
     */
    public void scaderePret(int a,Product t)
    {
    	if(t!=null)
    	{
    		t.micsoreazaPret(a);
    		scaderePret(a,t.left);
    		scaderePret(a,t.right);
    	}	
    }
    
    /**
     * Metoda inchide produsul (nu mai exista produse in depozit)
     */
    public void Inchide()
    {root=null;}
    
    
    public float getPret(String s)
    {
    	Product t;
    	t=find(s,root);
    	
    	return t.getPretN();
    }
    
    public int getCantit(String s)
    {
    	Product t;
    	t=find(s,root);
    	if(t!=null)
    	return t.getNr();
    	else return 0;
    }
}
