package tema3var2;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

//import Logica.Product;
// Logica.Warehouse;


/**
 * Clasa creeaza un panou pe care adauga toate produsele din depozit
 * Clasa satifface si cerintele de filtre asupra obiectelor
 * @author Sorana
 *
 */

public class Catalog extends JPanel
{
	private Warehouse wh;
	private ArrayList<Product> arr;
	private ImageIcon pic1,pic2;
	private int indice=0;
	
	/**
	 * Constructorul clasei Catalog 
	 * Setarea culorii, marimii panoului si a aranjarii obiectelor in panou
	 * @param wh - instanta depozitului caruia ii descriem obiectele
	 */
	Catalog(Warehouse wh)
	{
		this.wh=wh;
		setPreferredSize(new Dimension(800, 400));
  		setBackground(Color.black);
  		setLayout(null);
	}
	
	/**
	 * Metoda paint este mostenita din clasa JPanel si este suprascrisa
	 * Deseneaza componentele necesare unui catalog
	 */
	public void paint(Graphics g)
    {   super.paint(g);   
        Graphics2D g2 = (Graphics2D) g;
        int i,p;
  		g2.setColor(Color.magenta);
		g2.setFont(new Font("Tahoma", Font.PLAIN, 20)); 
  		if(arr.size()==0) 
  			  {g2.drawString("Ne pare rau, nici un produs disponibil momentan!", 150, 150);
  			   g2.drawString("Reveniti maine sau mai tarziu.", 225, 200);
  			   g2.drawString("O zi buna!", 325, 250);
  			  }
  		else g2.drawLine(400,0,400,400);
  		
  		pic1 = new ImageIcon("super.jpg");
  		pic2 = new ImageIcon("oferta.jpg");
  		for(i=indice;i<arr.size()&&i<indice+4;i++)
  		{
  			p=(int )((arr.get(i).getPretV()-arr.get(i).getPretN())*100/arr.get(i).getPretV());
  			switch(i-indice)
  			{
  			case 0:
  				g2.setColor(Color.magenta);
  				    g2.drawString("Denumire:"+arr.get(i).denumire, 20, 75);
  				    g2.drawString("Companie:"+arr.get(i).getCompanie(), 20, 100);
  				    g2.drawString("Pret:"+arr.get(i).getPretN()+"$", 40, 125);
  				    if(arr.get(i).getNr()==0)
  				      g2.drawString("Stoc nul", 60, 150);
  				    else
  				    	if(arr.get(i).getNr()<5)
  	  				      g2.drawString("UNDERstock", 60, 150);
  	  				    else
  	  				    	if(arr.get(i).getNr()<15)
  	  				    	g2.drawString("INstock", 60, 150);
  	  				    	else
  	  				      g2.drawString("OVERstock", 60, 150);
  				    if(arr.get(i).getPretV()!=-1)
  				    {
  				    	if(p>0)
  		  			       pic2.paintIcon(this, g,225,50);
  				    	if(p>=10)
  		  		           pic1.paintIcon(this, g,215,115);
  				    	g2.setColor(Color.white);
  				    	if(p>0) g2.drawString("-"+p+"%", 300, 65);
  				    }
  				 break;
  			case 1:g2.setColor(Color.magenta);
				    g2.drawString("Denumire:"+arr.get(i).denumire, 175, 250);
				    g2.drawString("Companie:"+arr.get(i).getCompanie(), 175, 275);
				    g2.drawString("Pret:"+arr.get(i).getPretN()+"$", 200, 300);
				    if(arr.get(i).getNr()==0)
	  				      g2.drawString("Stoc nul", 225, 325);
	  				    else
	  				    	if(arr.get(i).getNr()<5)
	  	  				      g2.drawString("UNDERstock", 225, 325);
	  	  				    else
	  	  				    if(arr.get(i).getNr()<15)
	  	  				    	g2.drawString("INstock", 225, 325);
	  	  				    	else
	  	  				      g2.drawString("OVERstock", 225, 325);
				    if(arr.get(i).getPretV()!=-1)
  				    {
  				    	if(p>0)
  		  			       pic2.paintIcon(this, g,25,250);
  				    	if(p>=10)
  		  		           pic1.paintIcon(this, g,15,315);
  				    	g2.setColor(Color.white);
  				    	if(p>0) g2.drawString("-"+p+"%", 100, 270);
  				    }
		  			
				 break;
  			case 2:
  				g2.setColor(Color.magenta);
  				    g2.drawString("Denumire:"+arr.get(i).denumire, 420, 75);
				    g2.drawString("Companie:"+arr.get(i).getCompanie(), 420, 100);
				    g2.drawString("Pret:"+arr.get(i).getPretN()+"$", 440, 125);
				    if(arr.get(i).getNr()==0)
	  				      g2.drawString("Stoc nul", 460, 150);
	  				    else
	  				    	if(arr.get(i).getNr()<5)
	  	  				      g2.drawString("UNDERstock", 460, 150);
	  	  				    else
	  	  				    if(arr.get(i).getNr()<15)
	  	  				    	g2.drawString("INstock", 460, 150);
	  	  				    	else
	  	  				      g2.drawString("OVERstock", 460, 150);
				    if(arr.get(i).getPretV()!=-1)
  				    {
  				    	if(p>0)
  		  			       pic2.paintIcon(this, g,625,50);
  				    	if(p>=10)
  		  		           pic1.paintIcon(this, g,615,115);
  				    	g2.setColor(Color.white);
  				    	if(p>0) g2.drawString("-"+p+"%", 700, 65);
  				    }
				 break;
  			case 3:
  				g2.setColor(Color.magenta);
  				g2.drawString("Denumire:"+arr.get(i).denumire, 575, 250);
			    g2.drawString("Companie:"+arr.get(i).getCompanie(), 575, 275);
			    g2.drawString("Pret:"+arr.get(i).getPretN()+"$", 600, 300);
			    if(arr.get(i).getNr()==0)
				      g2.drawString("Stoc nul", 625, 325);
				    else
				    	if(arr.get(i).getNr()<5)
	  				      g2.drawString("UNDERstock", 625, 325);
	  				    else
	  				    	if(arr.get(i).getNr()<15)
	  	  				    	g2.drawString("INstock", 625, 325);
	  	  				    	else
	  				      g2.drawString("OVERstock", 625, 325);
			    if(arr.get(i).getPretV()!=-1)
				    {
				    	if(p>0)
		  			       pic2.paintIcon(this, g,425,250);
				    	if(p>=10)
		  		           pic1.paintIcon(this, g,415,315);
				    	g2.setColor(Color.white);
				    	if(p>0) g2.drawString("-"+p+"%", 500, 270);
				    }
				 break;
  			
  			default: break;
  			}
  		}
       	
    }
	
	
	  
	/**
	 * Transformarea unui arbore binar de cautare intr-o lista
	 * Parcurgerea se face in PREORDINE 
	 * @param t Produsul din depozit la care s-a ajuns
	 * @param sel =metoda de selectie a produsului
	 * @param prMax =pretl maxim (luat in considerare daca sel=1)
	 * @param compan =compania dorita (luat in considerare daca sel=2)
	 */
	public void ArboreToLista(Product t,int sel,float prMax,String compan)
	{
		if(t!=null)
		{
			switch(sel)
			{
			 case 1:  arr.add(t); break;
			 case 2:  if(t.getPretN()<=prMax) 
				   arr.add(t); break;
			 case 3:  if(compan.hashCode()==t.getCompanie().hashCode()) 
				           arr.add(t); break;
			 default : break;
			}
			ArboreToLista(t.left,sel,prMax,compan);
			ArboreToLista(t.right,sel,prMax,compan);
		}
	}

	
	/**
	 * Metoda adauga pe ecran cele 2 butoane : Stanga . Drapta
	 * @param sel =metoda de selectie a produsului
	 * @param prMax =pretl maxim (luat in considerare daca sel=1)
	 * @param compan =compania dorita (luat in considerare daca sel=2)
	 */
	public void afis(int sel,float prMax,String compan)
	{ 
		indice=0;
		JButton stanga=new JButton("<<");
	    JButton dreapta=new JButton(">>");
	    arr=new ArrayList();
		ArboreToLista(wh.root,sel,prMax,compan);
		
		
		
		class St implements ActionListener
	     {
	        public void actionPerformed(ActionEvent event)
	        {    
	         if(indice>=4)	
	         {	 indice=indice-4;
	       	 repaint();}
	        }
	     }
		class Dr implements ActionListener
	     {
	        public void actionPerformed(ActionEvent event)
	        {    
	         if((indice+4)<arr.size())	
	         { indice=indice+4;
	       	 repaint();}
	        }
	     }
		
		
		ActionListener ascult1 = new St();
	    stanga.addActionListener(ascult1);
	    ActionListener ascult2 = new Dr();
	    dreapta.addActionListener(ascult2);
		
		stanga.setBounds(25,25,50,25);
		this.add(stanga);
		dreapta.setBounds(715,25,50,25);
		this.add(dreapta);
		repaint();
	}
}
