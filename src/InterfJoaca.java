package tema3var2;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JPanel;



/**
 * Clasa  creeaza un panou asupra caruia se definesc acultatori de tastatura 
 * Se creeaza o interfata grafica cu ajutorul careia clientul activa va putea adauga produse in cos
 * @author Sorana
 *
 */

public class InterfJoaca  extends JPanel implements KeyListener
{
	private int x,y;
	private ImageIcon pic1,pic2;
	private int dir=0;
	private Warehouse wh;
	private ArrayList<Product> arr=new ArrayList();
	private int ind=0,cl;
	Panou2 p2;
	Customer cust;
	
	/**
	 * Cosntructorul clasei seteaza atributele primite, culoarea, dimensiunea si adauga ascultatorii de evenimente
	 * @param h --depozitul din care se pot cumpara produse
	 * @param q --clientul care cumpara
	 */
	 public InterfJoaca(Warehouse h,Customer q)
	    { 
	       setPreferredSize(new Dimension(700, 500));
	       setBackground(Color.black);
	      this.addKeyListener(this);
	      setFocusable(true);
	      x=2;
	      y=1;
	      dir=0;
	      wh=h;
	      this.cust=q;
	      
	    }

	 /**
	  * Metoda paint adauga elemntele grafice dorite pe panou
	  * Este apelata automat la fiecare modificare a continutului panoului
	  */
	    public void paint(Graphics g)
	    {  
	    	 
	        super.paint(g);
	        Graphics2D g2 =( Graphics2D)g;
	        g2.setColor(Color.magenta);
	        g.setColor(Color.DARK_GRAY);
			g2.setFont(new Font("Tahoma", Font.PLAIN, 16));
	        pic1 = new ImageIcon("strumf1.png");
	  		pic2 = new ImageIcon("strumf2.png");
	  		
	  		if(ind>7)
	  			{g.drawRect(0,0, 100,100);
	  			 g2.setColor(Color.BLUE);
	  		    g2.drawString("DownStairs", 15,50);
	  			}
	  		 g.setColor(Color.DARK_GRAY);
	  		if((ind+10)<arr.size() )
	  			{g.drawRect(500,0, 100,100);
	  			 g2.setColor(Color.blue);
	  		     g2.drawString("UpStairs", 515, 50);
	  			}
	  		 		
	  		if(dir==1)
	  		   pic2.paintIcon(this, g,x*pic2.getIconWidth(),10+y*pic2.getIconHeight());
	  		else 
	  		  pic1.paintIcon(this, g,x*pic1.getIconWidth(),10+y*pic1.getIconHeight());
	  		
	  		for(int i=1;i<=5;i++)
	  			{ g.setColor(Color.DARK_GRAY); 
	  			g.drawRect(i*pic1.getIconWidth(),180, pic1.getIconWidth(),100);
	  			   if((ind+i-1)<arr.size())
	  			   {  g2.setColor(Color.magenta);
	  				   g2.drawString(arr.get(ind+i-1).denumire,i*pic1.getIconWidth()+10,225);
	  			     g2.drawString(arr.get(ind+i-1).getPretN()+"$",i*pic1.getIconWidth()+10,250);
	  			   }
	  			 g.setColor(Color.DARK_GRAY);
	  		       g.drawRect(i*pic1.getIconWidth(),360, pic1.getIconWidth(),100);
	  		       if((ind+i+4)<arr.size())
	  		       {  g2.setColor(Color.magenta);
	  		    	   g2.drawString(arr.get(ind+i+4).denumire,i*pic1.getIconWidth()+10,400);
	  		         g2.drawString(arr.get(ind+i+4).getPretN()+"$",i*pic1.getIconWidth()+10,425);
	  		       }
	  			}
	  		
	    }
	    
	    /**
		 * Transformarea unui arbore binar de cautare intr-o lista
		 * Parcurgerea se face in PREORDINE
		 * @param t -produsul la care se ajunge
		 */
		public void ArboreToLista(Product t)
		{
			if(t!=null)
			{
				arr.add(t); 
				ArboreToLista(t.left);
				ArboreToLista(t.right);
			}
		}
	    
	    
	    public void afis(Panou2 p2,int cl)
	    {
	    	
	    	ArboreToLista(wh.root);
	    	this.p2=p2;
	    	this.cl=cl;
	    	repaint();
	    }
	  
	    
	 
		@Override
		public void keyPressed(KeyEvent e){}

		@Override
		/**
		 * Meotda este mostenita din clasa KeyListener si este suprascrisa 
		 * La fiecare eliberare uneia dintre sagetile de pe tastatura se verifica daca este posibila miscarea dorita,
		 * in caz afirmativ se vor modifica paramentrii corespunzatori si re apeleaza metoda de repaint
		 */
		public void keyReleased(KeyEvent e)
		{
			if(e.getKeyCode()==KeyEvent.VK_LEFT)
	         {  
		       dir=0;
		       if(x>0) x--;
		     }
		     if(e.getKeyCode()==KeyEvent.VK_RIGHT)
		     { 
		    	 dir=1;
			      if(x<7) x++;
		     }
		     if(e.getKeyCode()==KeyEvent.VK_DOWN)
		     { 
		        if(y<4) y++;
		     }
		     if(e.getKeyCode()==KeyEvent.VK_UP)
		     { 
		       if(y>0) y--;
		     }
		     
		     
		     repaint();
		     verifica();
		     repaint();
		}
		
		/**
		 * Metoda verifica daca utilizatorul a ajuns intr-o pozitie semnificativ
		 * In caz afirmativ sistemul executa comanda primita
		 */
		private void verifica()
		{
			if(x==0&&y==0&&ind>=10)
			{
				int select =(int)JOptionPane.showConfirmDialog(null,"Coborati scarile?", "??", JOptionPane.YES_NO_OPTION);
				if(select==0)
	              {
	            	 ind=ind-10;
	              }
			}
			if(x==6&&y==0&&(ind+10)<arr.size())
			{
				int select =(int)JOptionPane.showConfirmDialog(null,"Urcati scarile?", "??", JOptionPane.YES_NO_OPTION);
				if(select==0)
	              {
	            	 ind=ind+10;
	              }
			}
			int q;
		     if(y==2&&x>=1&x<=5)
		        { q=ind+x-1;
		          if(q<arr.size())
		          {
		        	  int select =(int)JOptionPane.showConfirmDialog(null,"Cumparati "+arr.get(q).denumire, "??", JOptionPane.YES_NO_OPTION);
					  if(select==0)
			            {
						  	cust.addItem(arr.get(q).denumire,1);
						  	p2.desen(3, cl);
			            } 
		          }
		        }
		     if(y==4&&x>=1&x<=5)
		        { q=ind+x+4;
		          if(q<arr.size())
		          {
		        	  int select =(int)JOptionPane.showConfirmDialog(null,"Cumparati "+arr.get(q).denumire, "??", JOptionPane.YES_NO_OPTION);
					  if(select==0)
			            {
						  cust.addItem(arr.get(q).denumire,1);
						  p2.desen(3, cl);
			            } 
		          }
		        }
			
		}

		@Override
		public void keyTyped(KeyEvent arg0) {}
	
}
