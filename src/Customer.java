package tema3var2;
import java.util.*;
import java.awt.*;

/**
 * Clasa defineste un cumparator 
 * Un client este identificat printr-un Id si printr-un vector de cumparaturi
 * @author So
 *
 */
public class Customer 
{
  private int ID;
  public Customer left;         // ramura stanga
  public Customer right;        // ramura dreapta
  /**
   * v.x este codul produsului v.y= catitatea 
   */
  private ArrayList<String> v;
  private ArrayList<Integer> v2;
  
  
  /**
   * Constructorul creeaza un nou client care inca nu are produse cumparate
   * @param ID id-ul nou
   */
  public Customer(int ID)
  {
	  this.ID=ID;
	  v=new ArrayList<String>();
	  v2=new ArrayList<Integer>();
	  left=right=null;
  }
  
  /**
   * Adauga un nou item comandat 
   * Daca producul cu acel cod exista atunci se mareste numarul de elemente dorite
   * @param cod
   * @param cantit
   */
  public void addItem(String denum,int cantit)
  {
	  int sem=0; //pp ca nu mai gasim acelasi cod in comanda
	  if(cantit!=0)
	  { 
	  for(int i=0;i<v.size()&&sem==0;i++)
	   if(v.get(i).hashCode()==denum.hashCode())  
	   {
		   v2.set(i, v2.get(i)+cantit);
		   sem=1;
	   }
	  if(sem==0)
	  {	 v.add(denum);
	    v2.add(cantit);
	  }
	  }
  }
  
  /**
   * Se sterge un item din comanda
   * @param cod codul pe care il stergem 
   * @return
   */
  public boolean removeItem(String s)
  {
	  int sem=0; //pp ca nu gasim codul
	  for(int i=0;i<v.size()&&sem==0;i++)
	   if(v.get(i).hashCode()==s.hashCode())  
	   {
		   v.remove(i);
		   v2.remove(i);
		   sem=1;
	   }
	  if(sem==0)
        	 return false;
	  else return true;
  }
  
  /**
   * Metoda actualizeaza cantitatea unui produs cu codul COD
   * Daca cantitatea noua este 0 atunci se sterge codul din lista de comanda
   * @param cod
   * @param cant nou cantitate
   * @return true daca am gasit codul , false altfel
   */
  public boolean actualizeaza(String s,int cant)
  {
	  for(int i=0;i<v.size();i++)
	   if(v.get(i).hashCode()==s.hashCode())  
	   {
		   if(cant==0)
		   { v.remove(i);
		   v2.remove(i);
		   }
		   else v2.set(i,cant);
		   return true;
	   }
	  return false;
  }
  
  /**
   * Metoda returneaza codul produsului cu indice index
   * @param index
   * @return
   */
  public String getDenum(int index)
  {
	  return v.get(index);
  }
  
  
  /**
   * Metode de gettter si setter
   */
  public int getNrProd()
  { return v.size();}
  
public int getID()
{return ID;}

public void setID(int a)
{ID=a;}

public String getDenumItem(int i)
{
   return v.get(i);	
}

public int getCantItem(int i)
{
   return v2.get(i);	
}

public void setCantItem(int i,int cantit)
{
   v2.set(i,cantit);
}

public ArrayList<String> getDenum()
{return v;}

public ArrayList<Integer> getCantit()
{return v2;}


public void setCantit(ArrayList<Integer> q)
{ v2=q;}

public void setDenum(ArrayList<String> q)
{ v=q;}


}