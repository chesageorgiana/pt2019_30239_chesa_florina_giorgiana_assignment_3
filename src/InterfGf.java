package tema3var2;


import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

//import Logica.OPDept;

/**
 * Clasa creeaza un panou pe care sunt adaugate compenente grafice cu ajutorul carora utilizatorul interactioneaza cu sistemul
 * Crearea panoului principal
 * @author SoRana
 *
 */

public class InterfGf extends JPanel
{
	OPDept op;
	Panou2 p2;
	private Catalog cat;
	private int ClientActiv=-1;
	InterfJoaca joc;
	private int nrCom=0;
	
	/**
	 * Constructorul instantiaza un nou depozit , catalog si a panoului care se adauga in dreapta panoului principal
	 * Setarea dimensiunii panoului si a modului de aranjare a componentelor
	 */
	 public InterfGf()
	    {
		 op=new OPDept();
		 p2=new Panou2(op);
		 cat=new Catalog(op.depozit);
	        setPreferredSize(new Dimension(1200, 700));
	        this.setLayout(new BorderLayout());
	    }

	
	 /**
	  * Metoda incepe instanteaza componentele necesare , le adauga pe ecran
	  * Creeaza clasele interne necesare interactionarii utilizatorului cu sistemul 
	  */
	public void incepe()
    {
		
     
     //creem butoanele  jlabel
     JButton but1=new JButton("Client nou");
     JButton but2=new JButton("Insereaza");
     JButton but3=new JButton("Elimina");
     JButton but4=new JButton("Cauta");
     JButton but5=new JButton("Actualizeaza");
     JButton but6=new JButton("Marirea preturi (%) ");
     JButton but7=new JButton("Micsorare preturi (%) ");
     JButton but8=new JButton("Adauga Item ");
     JButton but9=new JButton("Modificare comnda");
     JButton but10=new JButton("Calculare comanda");
     JButton but11=new JButton("Finalizare comanda");
     JButton but12=new JButton("Elimina Client");
     JButton but13=new JButton("Cauta Client");
     JButton but14=new JButton("Inchide Depozitul");
     JButton but15=new JButton("Rasfoieste Catalog");
     JButton but16=new JButton("Rasfoieste SuperPreturi");
     JButton but17=new JButton("Rasfoieste Companie");
     JButton but18=new JButton("Logare Client");
     JButton but19=new JButton("Incarca Depozitul");
     JButton but20=new JButton("Salveaza Depozitul");
     JButton but21=new JButton("Cumpara usor! Cumpara jucandu-te!");
     
     
     JLabel box1=new JLabel("Operatii cu DEPOZITUL");
     JLabel box2=new JLabel("Operatii cu CLIENTI");
     JLabel box3=new JLabel("Operatii asupra COMANZII");
     JLabel box4=new JLabel("Cantitate");
     final JLabel box5=new JLabel("Produsul cautat");
     final JLabel box6=new JLabel("");
     JLabel box8=new JLabel("Cantitate");
     final JLabel box9=new JLabel("");
     final JLabel sl1=new JLabel("");
     final JLabel sl2=new JLabel("");
     final JTextField text1=new JTextField("Denumire");
     final JTextField text2=new JTextField("Pret");
     final JTextField text3=new JTextField("Denumire");
     final JTextField text4=new JTextField("Companie");
     
     final JSlider slid1 = new JSlider(0, 200, 0);
     final JSlider slid2 = new JSlider(0, 200, 0);
     
    final JRadioButton radio1=new JRadioButton("Pret");;
    JRadioButton radio2=new JRadioButton("Cantitate");;
    ButtonGroup group = new ButtonGroup();
    group.add(radio1);
    group.add(radio2);
    
    
    
    op.incarcaDepozit();
    p2.desen(1);
    

     class InsertProd implements ActionListener
      {
         public void actionPerformed(ActionEvent event)
         {    
        	 String inputValue;
             float w1;
             inputValue =text2.getText();
                 try{
                	 w1=Float.parseFloat(inputValue);
                	 op.insertProd(text1.getText(),w1,slid1.getValue(),text4.getText());
                    }
                 catch(Exception e)
                 { JOptionPane.showMessageDialog(null, "Nu ati indrodus un pret format dintr-un numar real", "Error", JOptionPane.ERROR_MESSAGE);
                 }
              	 
                         
        	 repaint();
        	 p2.desen(1);
         }
      }
     class AdaugaItem implements ActionListener
     {
        public void actionPerformed(ActionEvent event)
        {    
       	 String inputValue;
            float w1;         
            
            if(ClientActiv==-1)
            	JOptionPane.showMessageDialog(null, "Va rugam logati-va inainte de a cumpara!", "Error", JOptionPane.ERROR_MESSAGE);
            else{
                   inputValue =text3.getText();
            
                   if(op.addItem(ClientActiv, text3.getText(), slid2.getValue()))
                   {
                	   p2.desen(3,ClientActiv);
                	   if(slid2.getValue()==0)
                    	   JOptionPane.showMessageDialog(null, "Produsele cu cantitati nule nu se adauga ", "INFO", JOptionPane.INFORMATION_MESSAGE);

                   }
                   else
                       JOptionPane.showMessageDialog(null, "Produsul nu exista ! ", "Error", JOptionPane.ERROR_MESSAGE);

                   
                 p2.desen(3,ClientActiv);        	 
               }           
       	 repaint();
        }
     }
     class ModifItem implements ActionListener
     {
        public void actionPerformed(ActionEvent event)
        {    
            if(ClientActiv==-1)
            	JOptionPane.showMessageDialog(null, "Va rugam logati-va inainte de a cumpara!", "Error", JOptionPane.ERROR_MESSAGE);
            else{
            	String inputValue = JOptionPane.showInputDialog("Introduceti denumirea produsului"); 
                int w;
                String inputValue2 = JOptionPane.showInputDialog("Introduceti cantitatea noua"); 
                        try{
                       	    w=Integer.parseInt(inputValue2);
                       	    if(!op.modifItem(ClientActiv,inputValue,w))
                       	    	JOptionPane.showMessageDialog(null, "Nu a fost gasit produsul "+inputValue, "Error", JOptionPane.ERROR_MESSAGE);
                           }
                        catch(Exception e)
                        {
                       	 JOptionPane.showMessageDialog(null, "Nu ati indrosus un numar", "Error", JOptionPane.ERROR_MESSAGE);
                        }	
                 p2.desen(3,ClientActiv);        	 
               }           
       	 repaint();
        }
     }
     class EliminaProd implements ActionListener
     {
        public void actionPerformed(ActionEvent event)
        {    
       	 String inputValue =text1.getText(); 
            if(op.eliminaProd(inputValue))
           		   JOptionPane.showMessageDialog(null,"Produsul a fost eliminat","Info",JOptionPane.PLAIN_MESSAGE);
           	   else
           		JOptionPane.showMessageDialog(null, "Produsul nu a fost gasit", "Error", JOptionPane.ERROR_MESSAGE);
             	 
       	 repaint();
       	 p2.desen(1);
        }
     }
     class ActualizProd implements ActionListener
     {
        public void actionPerformed(ActionEvent event)
        {    
        	String inputValue = JOptionPane.showInputDialog("Introduceti denumirea produsului"); 
            int w2;
            float w1;
            if(radio1.isSelected() ) //se modif pretul	
           	    {
           	    	String inputValue2 = JOptionPane.showInputDialog("Introduceti pretul nou"); 
                    try{
                   	    w1=Float.parseFloat(inputValue2);
                   	    if(!op.modifProdus(inputValue,w1,-1))
                   	    	JOptionPane.showMessageDialog(null, "Nu a fost gasit produsul "+inputValue, "Error", JOptionPane.ERROR_MESSAGE);
                       }
                    catch(Exception e)
                    {
                   	 JOptionPane.showMessageDialog(null, "Nu ati indrosus un numar", "Error", JOptionPane.ERROR_MESSAGE);
                    }	
           	    }
           	    else
           	    {
           	    	String inputValue2 = JOptionPane.showInputDialog("Introduceti cantitatea noua"); 
                    try{
                   	    w2=Integer.parseInt(inputValue2);
                   	    if(w2>0)
                   	    {
                   	    if(!op.modifProdus(inputValue,-1,w2))
                   	    	JOptionPane.showMessageDialog(null, "Nu a fost gasit produsul cu aces cod", "Error", JOptionPane.ERROR_MESSAGE);
                   	    }
                   	    else
                   	    	if(op.eliminaProd(inputValue))
                        		   JOptionPane.showMessageDialog(null,"Produsul a fost eliminat","Info",JOptionPane.PLAIN_MESSAGE);
                        	   else
                        		JOptionPane.showMessageDialog(null, "Produsul "+inputValue+"nu a fost gasit", "Error", JOptionPane.ERROR_MESSAGE);
                          	
                       }
                    catch(Exception e)
                    {
                   	 JOptionPane.showMessageDialog(null, "Nu ati indrosus un numar", "Error", JOptionPane.ERROR_MESSAGE);
                    }	
           	    	
           	    }
                           
         repaint();
       	 p2.desen(1);
        }
     }
     class CautaProd implements ActionListener
     {
        public void actionPerformed(ActionEvent event)
        {    
        	String inputValue = JOptionPane.showInputDialog("Introduceti codul produsului"); 
           
           	    box5.setText(op.cautaProd(inputValue));
           	    p2.desenCautat(1,inputValue,0);
              
       	 repaint();
        }
     }
     class MarireProd implements ActionListener
     {
        public void actionPerformed(ActionEvent event)
        {    
       	    String inputValue = JOptionPane.showInputDialog("Procentul de micsorare"); 
            int w;
            try{
           	   w=Integer.parseInt(inputValue);
                op.modifPret(w, 1);
                p2.desen(1);
               }
            catch(Exception e)
            {
           	 JOptionPane.showMessageDialog(null, "Nu ati indrosus un numar", "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
     }
     class MicsorareProd implements ActionListener
     {
        public void actionPerformed(ActionEvent event)
        {    
       	    String inputValue = JOptionPane.showInputDialog("Procentul de micsorare"); 
            int w;
            try{
           	   w=Integer.parseInt(inputValue);
                op.modifPret(w, 0);
                p2.desen(1);
               }
            catch(Exception e)
            {
           	 JOptionPane.showMessageDialog(null, "Nu ati indrosus un numar", "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
     }
     
     class Client implements ActionListener
      {
         public void actionPerformed(ActionEvent event)
         {    
        	 String inputValue = JOptionPane.showInputDialog("Introduceti ID-ul noului client"); 
             int w;
             try{
            	 w=Integer.parseInt(inputValue);
            	 op.insertClient(w);
                 p2.desen(2);
                }
             catch(Exception e)
             {
            	 JOptionPane.showMessageDialog(null, "Nu ati indrodus un numar", "Error", JOptionPane.ERROR_MESSAGE);
             }
         }
      }
     class Logare implements ActionListener
     {
        public void actionPerformed(ActionEvent event)
        {    
       	 String inputValue = JOptionPane.showInputDialog("Introduceti ID-ul noului client"); 
            int w;
            try{
           	 w=Integer.parseInt(inputValue);
           	if(op.cautaClient(w))
       	    {
       	       ClientActiv=w;
       	       box6.setText("Client activ: "+w);
       	    }
       	    else 
       	    {	
           	    p2.desen(2);
       	    	JOptionPane.showMessageDialog(null, "Nu exista client cu Id-ul "+w, "Error", JOptionPane.ERROR_MESSAGE);
       	       
       	    }

               }
            catch(Exception e)
            {
           	 JOptionPane.showMessageDialog(null, "Nu ati indrosus un numar", "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
     }
     class EliminaClient implements ActionListener
     {
        public void actionPerformed(ActionEvent event)
        {    
         String inputValue = JOptionPane.showInputDialog("Introduceti Id-ul clientului care va fi scos din sistem"); 
            int w;
            try{
           	    w=Integer.parseInt(inputValue);
           	 if(op.eliminaClient(w))
         		   JOptionPane.showMessageDialog(null,"Clientul a fost scos din sistem","Info",JOptionPane.PLAIN_MESSAGE);
         	   else
         		JOptionPane.showMessageDialog(null, "Clientul nu a fost gasit", "Error", JOptionPane.ERROR_MESSAGE);
           	
              	 p2.desen(2);
               }
            catch(Exception e)
            {
           	 JOptionPane.showMessageDialog(null, "Nu ati indrosus un numar", "Error", JOptionPane.ERROR_MESSAGE);
            }
       	 repaint();
        }
     }
     class CautaClient implements ActionListener
     {
        public void actionPerformed(ActionEvent event)
        {    
        	String inputValue = JOptionPane.showInputDialog("Introduceti Id-ul clientului"); 
            int w;
            try{
           	    w=Integer.parseInt(inputValue);
           	    if(op.cautaClient(w))
           	    {
           	       p2.desenCautat(2,"",w);
           	    }
           	    else 
           	    {	
               	    p2.desen(2);
           	    	JOptionPane.showMessageDialog(null, "Nu exista client cu Id-ul "+w, "Error", JOptionPane.ERROR_MESSAGE);
           	       
           	    }
               }
            catch(Exception e)
            {
           	 JOptionPane.showMessageDialog(null, "Nu ati indrosus un numar", "Error", JOptionPane.ERROR_MESSAGE);
            }
       	 repaint();
        }
     }
     class Catalog1 implements ActionListener
     {
        public void actionPerformed(ActionEvent event)
        {    
        	JFrame fr = new JFrame();
      	    
       	 fr.setTitle("Catalog produse");
       	 fr.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
       	    
       	  fr.setContentPane(cat);

      	  cat.afis(1,0,"");
       	    
       	  fr.setSize(800, 400);
       	  fr.setVisible(true); 
        	
       	 repaint();
        }
     }
     class Catalog2 implements ActionListener
     {
        public void actionPerformed(ActionEvent event)
        {    
        	
        	String inputValue = JOptionPane.showInputDialog("Introduceti pretul MAXIM dorit"); 
            try{
           	 float w1=Float.parseFloat(inputValue);
           	 JFrame fr = new JFrame();
       	    
           	 fr.setTitle("Catalog produse");
           	 fr.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
           	    
           	 fr.setContentPane(cat);

          	  cat.afis(2,w1,"");
           	    
           	  fr.setSize(800, 400);
           	  fr.setVisible(true); 
            	
           	   }
            catch(Exception e)
            {
           	 JOptionPane.showMessageDialog(null, "Nu ati indrosus un numar", "Error", JOptionPane.ERROR_MESSAGE);
            }	
        	
        	
       	 repaint();
        }
     }
     class Catalog3 implements ActionListener
     {
        public void actionPerformed(ActionEvent event)
        {    
        	
        	String inputValue = JOptionPane.showInputDialog("Introduceti pretul nou"); 
            
           	 JFrame fr = new JFrame();
       	    
           	 fr.setTitle("Catalog produse");
           	 fr.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
           	    
           	 fr.setContentPane(cat);

          	  cat.afis(3,0,inputValue);
           	    
           	  fr.setSize(800, 400);
           	  fr.setVisible(true); 
            	     	
       	 repaint();
        }
    }
     class Joaca implements ActionListener
     {
        public void actionPerformed(ActionEvent event)
        {    
        	JFrame fr = new JFrame();
        	if(ClientActiv!=-1)
        	{
       	       fr.setTitle("Cumpara usor");
       	       fr.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
         	    
       	        joc=new InterfJoaca(op.depozit,op.Client(ClientActiv));
       	 
       	        fr.setContentPane(joc);

      	        joc.afis(p2,ClientActiv);
       	    
       	         fr.setSize(700, 500);
       	         fr.setVisible(true); 
           	
            	 
       	      repaint();
       	    
        	}
        	else  JOptionPane.showMessageDialog(null, "Nici un client activ", "Error", JOptionPane.ERROR_MESSAGE);
            
        }
     }
    class IncarcaDep implements ActionListener
     {
        public void actionPerformed(ActionEvent event)
        {    
        
        op.depozit.Inchide();
        op.incarcaDepozit();
        p2.desen(1);
       	 repaint();
        }
    }
    class SalvDep implements ActionListener
    {
       public void actionPerformed(ActionEvent event)
       {    
       	
       op.salveazaDepozit();
      	 repaint();
       }
   }
    class CalcComanda implements ActionListener
    {
       public void actionPerformed(ActionEvent event)
       {    
    	   
    	   float q=op.calcCom(ClientActiv);
    	   if(ClientActiv!=-1)
    	   box9.setText("Valoarea Comenzii pentru clientul "+ClientActiv+" este :"+q+"$");
    	   else box9.setText("Nici un client logat!");
     	  
       p2.desen(3,ClientActiv);
      	 repaint();
       }
   }
    class FinalizComanda implements ActionListener
    {
       public void actionPerformed(ActionEvent event)
       {    
    	   
    	   if(op.verifStock(ClientActiv))
    	   {
    		   nrCom++; 
    		   op.terminareComanda(ClientActiv, nrCom, -1);
    	   }
    	   else 
    	   {
    		   int select =(int)JOptionPane.showConfirmDialog(null, "                         Pe stoc nu se gasesc toate produsele soliciate.\nApasati YES pentru continuarea comenzii pentru toate produsele existente pe stock. \n           Apasati NO pentru selectarea doar a prosuselor cu stocul complet \n                           Apasati CANCEL pentru anularea facutararii", "Error", JOptionPane.YES_NO_CANCEL_OPTION);
              if(select==0)
              {
            	 nrCom++; 
            	 op.terminareComanda(ClientActiv, nrCom, 0);
              }
              else
            	if(select==1)
    	         {
            		
            		nrCom++;
            		 op.terminareComanda(ClientActiv, nrCom, 1);
    	         }
    	   }
    	   
    	   
    	   
         p2.desen(3,ClientActiv);
      	 repaint();
       }
   }
    class Slide1 implements ChangeListener
     {  
        public void stateChanged(ChangeEvent event)
        {  
            sl1.setText(slid1.getValue()+"");
        }
     }
    class Slide2 implements ChangeListener
    {  
       public void stateChanged(ChangeEvent event)
       {  
           sl2.setText(slid2.getValue()+"");
       }
    }
    class InchideDepoz implements ActionListener
    {
       public void actionPerformed(ActionEvent event)
       {    
    	 op.depozit.Inchide();  
      	 p2.desen(1);
       }
    }
         
     
     ChangeListener listener = new Slide1();
     slid1.addChangeListener(listener);
     ChangeListener listener2 = new Slide2();
     slid2.addChangeListener(listener2);
     
     ActionListener ascult1 = new Client();
     but1.addActionListener(ascult1);
     ActionListener ascult2 = new InsertProd();
     but2.addActionListener(ascult2);
     ActionListener ascult3 = new EliminaProd();
     but3.addActionListener(ascult3);
     ActionListener ascult4 = new CautaProd();
     but4.addActionListener(ascult4);
     ActionListener ascult5 = new ActualizProd();
     but5.addActionListener(ascult5);
     ActionListener ascult6 = new MarireProd();
     but6.addActionListener(ascult6);
     ActionListener ascult7 = new MicsorareProd();
     but7.addActionListener(ascult7);
     ActionListener ascult8 = new InchideDepoz();
     but14.addActionListener(ascult8);
     ActionListener ascult9 = new EliminaClient();
     but12.addActionListener(ascult9);
     ActionListener ascult10 = new CautaClient();
     but13.addActionListener(ascult10);
     ActionListener ascult11 = new Catalog1();
     but15.addActionListener(ascult11);
     ActionListener ascult12 = new Catalog2();
     but16.addActionListener(ascult12);
     ActionListener ascult13 = new Catalog3();
     but17.addActionListener(ascult13);
     ActionListener ascult14 = new Logare();
     but18.addActionListener(ascult14);
     ActionListener ascult15 = new IncarcaDep();
     but19.addActionListener(ascult15);
     ActionListener ascult16 = new SalvDep();
     but20.addActionListener(ascult16);
     ActionListener ascult17 = new AdaugaItem();
     but8.addActionListener(ascult17);
     ActionListener ascult18 = new ModifItem();
     but9.addActionListener(ascult18);
     ActionListener ascult19 = new CalcComanda();
     but10.addActionListener(ascult19);
     ActionListener ascult20 = new FinalizComanda();
     but11.addActionListener(ascult20);
     ActionListener ascult21 = new Joaca();
     but21.addActionListener(ascult21);
     
     
        
     
   
     //Elemente lucrul cu depozitul
     box1.setBounds(150,0,200,25);
     this.add(box1);
     box4.setBounds(150,35,100,25);
     this.add(box4);
     slid1.setBounds(225,35,150,25);
     this.add(slid1);
     text1.setBounds(15,35,80,25);
     this.add(text1);
     text2.setBounds(95,35,45,25);
     this.add(text2);
     text4.setBounds(15,60,125,25);
     this.add(text4);
     but2.setBounds(375,15,100,25);
     this.add(but2);
     but3.setBounds(375,55,100,25);
     this.add(but3);
     but4.setBounds(15,85,100,25);
     this.add(but4);
     box5.setBounds(150,85,400,25);
     this.add(box5);
     but5.setBounds(15,115,150,25);
     this.add(but5);
     but6.setBounds(15,145,175,25);
     this.add(but6);
     but7.setBounds(215,145,175,25);
     this.add(but7);
     radio1.setBounds(200,115,75,25);
     radio1.setSelected(true);
     this.add(radio1);
     radio2.setBounds(275,115,100,25);
     this.add(radio2);
     sl1.setBounds(355,15,50,25);
     this.add(sl1);
     but19.setBounds(15,175,145,25);
     this.add(but19);
     but20.setBounds(174,175,145,25);
     this.add(but20);
     but14.setBounds(335,175,145,25);
     this.add(but14);
     
     
     //operatii cu clienti
     box2.setBounds(150,200,200,25);
     this.add(box2);
     but12.setBounds(15,277,200,25);
     this.add(but12);
     but1.setBounds(15,240,200,25);
     this.add(but1);
     but13.setBounds(15,314,200,25);
     this.add(but13);
     but15.setBounds(275,297,200,25);
     this.add(but15);
     but16.setBounds(275,334,200,25);
     this.add(but16);
     but17.setBounds(275,371,200,25);
     this.add(but17);
     but18.setBounds(15,351,200,25);
     this.add(but18);
     box6.setBounds(15,388,200,25);
     this.add(box6);
         
     
     //operatii asupa comenzii
     box3.setBounds(150,425,200,25);
     this.add(box3);
     text3.setBounds(15,475,80,25);
     this.add(text3);
     box8.setBounds(100,475,100,25);
     this.add(box8);
     slid2.setBounds(175,475,150,25);
     this.add(slid2);
     but8.setBounds(350,475,125,25);
     this.add(but8);
     but9.setBounds(14,510,200,25);
     this.add(but9);
     but10.setBounds(250,510,200,25);
     this.add(but10);
     but11.setBounds(250,580,200,25);
     this.add(but11);
     box9.setBounds(15,545,400,25);
     this.add(box9);
     sl2.setBounds(300,455,50,25);
     this.add(sl2);
     but21.setBounds(100,625,300,25);
     this.add(but21);
     
     
     
     this.add(new JLabel(""));
     this.add(p2,BorderLayout.EAST);
     
   
    }
}
