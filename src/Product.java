package tema3var2;

/**
 * Clasa produselor din depozit
 * @author Sorana
 *
 */

public class Product 
{
	private float pretN,pretV;
	private int nrItem;
	public String denumire;
	private String companie;
	public Product left;         // ramura stanga
    public Product right;        // ramura dreapta

	/**
	 * Constructor 1 petru cazul in care se trimit toate datele despre un produs
	 * Se seteaza de asemenea ca fii drept resectiv stang sunt nuli
	 * @param cod
	 * @param pr =pretul de inceput
	 * @param cantit
	 */
	public Product(String a,float pr,int cantit,String comp)
	{
		this.pretN=pr;
		this.pretV=-1;
		this.denumire=a; 
		this.nrItem=cantit;
		left = right = null;
		companie=comp;
		
	}
	
		
	/**
	 * Numarul de itemi creste cu n
	 * @param n numarul de produse adaugate
	 */
	public void incremNr(int n)
	{
		nrItem=nrItem+n;
	}
	/**
	 * Numarul de itemi scade cu n
	 * @param n numarul de produse eliminate
	 */
	public void decremNr(int n)
	{
		nrItem=nrItem-n;
	}
	
	/**
	 * Metoda actualizeaza pretul unui produs daca acesta creste cu x% 
	 * @param x =procentul de crestere
	 */
	public void marestePret(int x)
	{
		pretV=pretN;
		pretN=(100+x)*pretN/100;
	}
	
	/**
	 * Metoda actualizeaza pretul unui produs daca acesta scade cu x% 
	 * @param x =procentul de micsorare
	 */
	public void micsoreazaPret(int x)
	{
		pretV=pretN;
		pretN=(100-x)*pretN/100;
	}
	
	/**
	 * Metoda de accesare a pretului
	 * @return pretul actual al produsului
	 */
	public float  getPretN()
	{ return pretN; 	}
	public float  getPretV()
	{ return pretV; 	}
	
	/**
	 * Metoda accesoare pentru numarul de bucati din stoc
	 * @return numar bucati existente
	 */
	public int  getNr()
	{ return nrItem; 	}
	public void  setNr(int n)
	{ nrItem=n; 	}
	public void  setPretN(float n)
	{ pretV=pretN;
		pretN=n; 	}
	
	/**
	 * Setarea companiei
	 * @param a -numele comapaniei
	 */
	public void setCompanie(String a)
	{
		companie=a;
	}
	public String getCompanie()
	{return companie;}
}
