package tema3var2;

/**
 * Clasa implementeaza un depozit careare in spate un Arbore Binar de Cautare
 * Nodurile acestuia sunt instante ale clasei Produs
 * @author Sorana
 *
 */
public class Persoane 
{
	/** Radacina arborelui */
    public Customer rootP;
	
	public Persoane()
	{ rootP=null; }
	
	
	
	/**
	 * Inseararea unui nou client in arbore  
	 * @param ID =id-ul nou
	 * @return true daca am adaugat, false daca exista clientul cu id cerut
	 */
    public boolean adaugaClient(int ID) 
    {
       if(cauta(ID)) return false;
      Customer cust=new Customer(ID);	
       rootP = insert( cust, rootP );
       return true;
    }
    
   
    
    /**
     * Metoda interioara de inserare in arbore
     * Inserarea se face dupa codul fiecarui produs
     * @param x Produsul nou care se insereaza
     * @param t radacina arborelui
     * @return noua radacina
     */
    protected Customer insert( Customer x, Customer t )
    {
        if( t == null )
            t = x;
        else if( x.getID() < t.getID()  )
            t.left = insert( x, t.left );
        else if(x.getID() > t.getID()  )
            t.right = insert( x, t.right );
       
        return t;
    }
    
    /**
     * Cautarea unui clint dupa ID
     * @param ID
     * @return true= a fost false= nu a fost gasit
     */
    public boolean cauta(int ID)
    {
    	Customer x;
    	x=find(ID,rootP);
    	if(x!=null)
            return true;
    	else return false;
    	
    }
    
    /**
     * Sterge din arbore produsul cu codul mentionat
     * Pentru a putea sterge trebuie ca prima data sa gasim produsul cu respectivul cod
     * @param cod codul de inlaturat
     * @return true daca codul a existat si a putut fi sters ,false dca codul nu exista in arbore
     */
    public boolean sterge( int ID )
    {
    	Customer x;
    	x=find(ID,rootP);
    	if(x!=null)
          { rootP = remove( ID, rootP );
            return true;
          }
    	else return false;
    }
    
    /**
     * Metoda interioara de sters dintr-un subarbore
     * Deoarece nu se intra in acesta metoda pentru t-null , atunci putem sa sarim peste acea testare
     * @param cod =codul de sters 
     * @param t radacina subarborelui
     * @return noua radacina
     */
    protected Customer remove( int ID, Customer t )
    {
    	Customer x;
        if( ID < t.getID() )
            t.left = remove( ID, t.left );
        else if( ID > t.getID() )
            t.right = remove( ID, t.right );
        else 
        	if( t.left != null && t.right != null ) // Two children
            {
        		x=gasesteMin( t.right );
               t.setID(x.getID());
               t.setCantit(x.getCantit()); //mutam comanda si id-ul
               t.setDenum(x.getDenum());
               t.right = stergeMin( t.right );
            } 
        	else
               t = ( t.left != null ) ? t.left : t.right;
        return t;
    }
    
    /**
     * Gasirea celui mai mic cod din subarbore
     * @param t radacina subarborelui
     * @return nodul cu cel mai mic cod
     */
    protected Customer gasesteMin( Customer t) 
    {
        if( t != null )
            while( t.left != null )
                t = t.left;
        
        return t;
    }
    
    /**
     * Stergerea minimului dintr=un subarbore
     * @param t radacina sunarb
     * @return noua rad
     */
    protected Customer stergeMin( Customer t ) 
    {
        if( t.left != null )
        {
            t.left = stergeMin( t.left );
            return t;
        } else
            return t.right;
    }
    
    
    /**
     * Metoda de gasire a unui client
     * @param ID =Id-ul cautat
     * @param t radacina arborelui
     * @return nodul cu Id-ul cautat
     */
    public Customer find( int ID , Customer t ) 
    {
        while( t != null ) 
        {
            if( ID < t.getID() )
                t = t.left;
            else 
            	if( ID > t.getID() )
                t = t.right;
            else
                return t;    // gasit
        }
        
        return null;         // nu l-am gasit
    }
    
    /**
     * Arborele va fi logic gol
     */
    public void pleacaClienti( ) 
    {
        rootP = null;
    }
    
    /**
     * Testeaza daca exista cleinti in arbore
     * @return true daca e gol, false altfel
     */
    public boolean existaClienti( ) 
    {
    	if(rootP==null)
        return false;
    	else return true;
    }
	
}