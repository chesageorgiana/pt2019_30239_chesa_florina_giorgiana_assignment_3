package tema3var2;
import javax.swing.JFrame;


/**
 * Clasa creeaza Frame-ul principal si adauga panoul principal de interactiune a utilizatorului cu aplicatia
 * @author So
 *
 */
public class Principal
{
    public static void main(String[] args)
    {
        JFrame window = new JFrame();
        
        window.setTitle("Simulare Cozi");
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        InterfGf p1=new InterfGf();
        window.setContentPane(p1);
        p1.incepe();
        
        window.setSize(1200, 700);
        window.setVisible(true);
    }
}