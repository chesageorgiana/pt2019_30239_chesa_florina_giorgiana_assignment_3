package tema3var2;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Line2D;
import java.util.ArrayList;

import javax.swing.JPanel;



/**
 * Clasa creeaza un panou care va fi adaugat in dreapta panoului principal 
 * Panoul creet ilustreaza in fiecare moment continutul sectiunii dorite:depozit , clienti sau cumparaturile unui client
 * @author So
 *
 */
public class Panou2 extends JPanel
{
	int tip,CautatCL,ClActiv;
	String cautat;
	boolean cautare;
	OPDept op;
	
	/**
	 * Cosntructorul seteaza culoarea si dimensiunea dorita
	 * @param op -instanta a departamentului de procesare a comenzilor
	 */
	public Panou2(OPDept op)
	{
		setPreferredSize(new Dimension(700, 500));
        setBackground(Color.pink);
        this.op=op;
	}
	
	/**
	 * Metoda paint adauga pe panou elementele dorite
	 * Apeleaza la randul sau functii care adauga doar acele elemente dorite la un moment dat
	 */
	 public void paint(Graphics g)
	    {   super.paint(g);   // Necesar pentru fundal.
	        Graphics2D g2 = (Graphics2D) g;
	        int inalt;
			g2.setColor(Color.WHITE);
			g2.setFont(new Font("Tahoma", Font.PLAIN, 17)); 
	        
	        switch(tip)
	        {
	        
	        case 1: {  
	        		  g2.drawString("Arborele produselor",0,15);
	        	      inalt=getHeight()/10;
	        	      paint1(op.depozit.root,0,0,getWidth(),inalt,g);
	        	      } break;
	        case 2: 
	         {  
      		  g2.drawString("Arborele Clientilor",0,15);
      	      inalt=getHeight()/10;
      	      paint2(op.pers.rootP,0,0,getWidth(),inalt,g);
      	      } break;
	        case 3:
	        {
	        	if(ClActiv!=-1)
	        	{g2.drawString("Cumparaturile solicitate de clientul "+ClActiv,0,15);
	        	Customer q;
	        	int i;
	        	q=op.pers.find(ClActiv, op.pers.rootP);
	        	ArrayList<String> a1=q.getDenum();
	        	ArrayList<Integer> a2=q.getCantit();
	      	   for(i=0;i<a1.size();i++)
	      		 g2.drawString(a1.get(i)+" : "+a2.get(i)+" buc ",15,25*i+50);
	        	}
	        	else g2.drawString("Nici un client logat",0,15);
	        }
	        default: break;
	        }
	       	
	    }
	
	 /**
	  * Metoda interna de adaugare elementelor dintr-un depozit
	  * @param t produsul la care s-a ajuns
	  * @param nivel -nivel la care se afla produsul
	  * @param x1 - extremitatea stanga pe axa Ox a locului in care se plaseaza String-ul cu numele produsului
	  * @param x2 - extremitatea dreapta pe axa Ox a locului in care se plaseaza String-ul cu numele produsului
	  * @param inalt -inaltimea dintre string-uti
	  * @param g instanta a clasei Graphics cu ajutorul caruia desenam
	  */
	 private void paint1(Product t,int nivel,int x1,int x2,int inalt,Graphics g)
	 {
		    Graphics2D g2 = (Graphics2D) g;
	        Line2D line;
	        int m=(x1+x2)/2;
	        if(t!=null)
	        {
	        	if(cautare &&t.denumire.hashCode()==cautat.hashCode())
	        	{
	        		g2.setColor(Color.white);
	        		g2.fillOval(m-20, nivel*inalt-10, 60, 60);
	        	}
	        	g2.setColor(Color.darkGray);
	        	g2.drawString(""+t.denumire, m-25, nivel*inalt+10);
	        	g2.drawString("("+t.denumire.hashCode()+")", m-35, nivel*inalt+30);
	        	if(t.right!=null)
	        	{
	        		g2.setColor(Color.LIGHT_GRAY);
	        		line = new Line2D.Double (m,inalt*nivel+10,(x2+m)/2,inalt*(nivel+1)+10);   
        			g2.draw(line);
	        		paint1(t.right,nivel+1,m,x2,inalt,g);
	        	}
	        	if(t.left!=null)
	        	{
	        		g2.setColor(Color.LIGHT_GRAY);
	        		line = new Line2D.Double (m,inalt*nivel+10,(x1+m)/2,inalt*(nivel+1)+10);   
        			g2.draw(line);
	        		paint1(t.left,nivel+1,x1,m,inalt,g);
	        	}
	        	
	        	
	        }	 
	 }
	 
	 /**
	  * Metoda interna de adaugare a Clientilor din sistem
	  * @param t clientul la care s-a ajuns
	  * @param nivel -nivel la care se afla produsul
	  * @param x1 - extremitatea stanga pe axa Ox a locului in care se plaseaza String-ul cu numele produsului
	  * @param x2 - extremitatea dreapta pe axa Ox a locului in care se plaseaza String-ul cu numele produsului
	  * @param inalt -inaltimea dintre string-uti
	  * @param g instanta a clasei Graphics cu ajutorul caruia desenam
	  */
	 private void paint2(Customer t,int nivel,int x1,int x2,int inalt,Graphics g)
	 {
		    Graphics2D g2 = (Graphics2D) g;
	        Line2D line;
	        int m=(x1+x2)/2;
	        if(t!=null)
	        {
	        	if(cautare &&t.getID()==CautatCL)
	        	{
	        		g2.setColor(Color.white);
	        		g2.fillOval(m-15, nivel*inalt-15, 40, 40);
	        	}
	        	g2.setColor(Color.darkGray);
	        	g2.drawString(""+t.getID(), m-10, nivel*inalt+10);
	        	if(t.right!=null)
	        	{
	        		g2.setColor(Color.LIGHT_GRAY);
	        		line = new Line2D.Double (m,inalt*nivel+10,(x2+m)/2,inalt*(nivel+1)+10);   
        			g2.draw(line);
	        		paint2(t.right,nivel+1,m,x2,inalt,g);
	        	}
	        	if(t.left!=null)
	        	{
	        		g2.setColor(Color.LIGHT_GRAY);
	        		line = new Line2D.Double (m,inalt*nivel+10,(x1+m)/2,inalt*(nivel+1)+10);   
        			g2.draw(line);
	        		paint2(t.left,nivel+1,x1,m,inalt,g);
	        	}
	        	
	        	
	        }	 
	 }
	 
	 /**
	  * Metoda care seteaza ce se doreste afisa
	  * @param  n=1 -> produsele din stoc
	  */
	public void desen(int n)
	{
		tip=n;
		cautare=false;
		repaint();
	}
	
/**
	  * Metoda care seteaza ce se doreste afisa
	  * @param  n=2 -> cleintii existenti
 * @param Cl -> clientul activ
 */
	public void desen(int n,int Cl)
	{
		tip=n;
		cautare=false;
		ClActiv=Cl;
		repaint();
	}
	
	/**
	  * Metoda care seteaza ce se doreste afisa
	  * @param  n=3 -> cautarea unui produs
      *@param s/a -> prodului / clientul cautat
*/
	public void desenCautat(int n,String s,int a)
	{
		tip=n;
		cautare=true;
		cautat=s;
		CautatCL=a;
		repaint();
	}
	
}
