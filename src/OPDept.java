package tema3var2;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.swing.JOptionPane;


/**
 * Clasa gestioneaza cleintii si comenzile acestora
 * @author So
 *
 */
public class OPDept 
{
	public Warehouse depozit;
	public Persoane pers;
	
	/**
	 * Constructorul instanteaza un obiecte din clasa Depozit si un obiecte din clasa Persoane
	 */
   public OPDept()
   {  depozit=new Warehouse();
	  pers=new Persoane();
   }
   
   /**
    * Metoda interna salveaza produsele existente pe stoc intr-un fisier
    * @param t -produsul la care s-a ajuns
    * @param fis -fisierul in care scriem
    */
   private void salveaza(Product t,PrintWriter fis)
   {

	   if(t!=null)
	   {
	       fis.println(t.denumire);
	       fis.println(t.getPretN()+"");
	       fis.println(t.getNr());
	       fis.println(t.getCompanie());
	       salveaza(t.left,fis);
		   salveaza(t.right,fis);  
	   }
	   
   }
   
   /**
    * Metoda publica de salvare a depozitului
    */
   public void salveazaDepozit()
   {  PrintWriter fis;
	  try{ fis = new PrintWriter(new FileWriter("depozit.txt"));
	       salveaza(depozit.root,fis);
	       fis.close();
	     }
	  catch(Exception e){}
   }
   
   
   /**
    * Metoda publica de incarcare a depozitului dintr-un fisier
    */
   public void incarcaDepozit()
   {
	   String linie,denum;
	   BufferedReader in;
	   float b;
	   int c;
	 try{  
		    in = new BufferedReader(new FileReader("depozit.txt"));
	 
		    while ((linie = in.readLine()) != null)
               {
		    	//fisierul este organizat pe linii
		    	//1linie :Denumire
		    	denum=linie;
		    	linie = in.readLine();
		    	//2 linie : pretul
		    	b=Float.parseFloat(linie);
		    	linie = in.readLine();
		    	//3 linie : cantitate
		    	c=Integer.parseInt(linie);
		    	linie = in.readLine();
		    	//4 linie : compania
		    	depozit.insereaza(denum, b, c,linie);
		    	
               }
	    }
	 catch(Exception e){}
	 
	 
     
   }
   
   /**
    * Inserarea unui produs in depozit
    * @param a -numele produsului
    * @param b -pretul produsului
    * @param c -cantitatea de produse
    * @param q -Compania producatoare
    */
   public void  insertProd(String a,float b,int c,String q)
   {
	   depozit.insereaza(a, b, c,q);	   
   }
   
   /**
    * Eliminarea unui produs din stoc
    * @param a numele produsului
    * @return true daca s-a putut realiza stergerea
    */
   public boolean eliminaProd(String a)
   {
	   return depozit.sterge(a);	   
   }
   
   /**
    * Cautarea unui produs din stoc
    * @param a numele produsului
    * @return true daca s-a gasit produsul
    */
   public String cautaProd(String a)
   {
	   return depozit.cauta(a);
   }
   
   /**
    * Modificarea pretului tuturor produseleor din stoc
    * @param a -procentul de modificare
    * @param q -1: marire ,altfel :micsorare
    */
   public void modifPret(int a,int q)
   { if(q==1) depozit.marirePret(a,depozit.root);
   else depozit.scaderePret(a,depozit.root);
   }
 
   /**
    * Modificarea cantitatii sau a pretului unui produs
    * @param a -denumirea produsului
    * @param b -pretul nou (pret =-1 inseamna ca se doreste modificarea cantitatii)
    * @param c -cantitatea noua
    * @return true daca s-a gasit produsul a s-a putut produce modificarea
    */
   public boolean modifProdus(String a,float b,int c)
   { 
	   if(b!=-1) return depozit.modifPret(a,b);
          else return depozit.modifCantit(a,c);
   }
   
   
   /**
    * Inserarea unui nou client
    * @param a -id-ul unui nou client
    */
   public void  insertClient(int a)
   {
	   	   pers.adaugaClient(a);
   }
   /**
    * Eliminarea unui Client
    * @param a -codul clientului
    * @return true daca s-a gasit si eliminat clientul
    */
   public boolean eliminaClient(int a)
   {
	   return pers.sterge(a);	   
   }
   
   /**
    * Cautarea unui client
    * @param a -id-ul clientului
    * @return true daca s-a gasit
    */
   public boolean cautaClient(int a)
   {
	   return pers.cauta(a);	   
   }
   
   /**
    * returneaza instanta clasei Customer la care se gaseste clientul
    * @param cl -id-ul clientului
    * @return
    */
   public Customer Client(int cl)
   {
	   return pers.find(cl, pers.rootP);
   }
   
   
   /**
    * Adaugarea unui item la cumparaturile clientului
    * @param ID -id-ul clientului
    * @param Denum -denumirea produsului
    * @param cantit -cantitatea produsului
    * @return true daca produsul exista si s-a putut adauga
    */
   public boolean addItem(int ID,String Denum,int cantit)
   {
	   Customer q;
	   q=pers.find(ID, pers.rootP);
	   if(depozit.find(Denum, depozit.root)!=null)
	   {
	   q.addItem(Denum, cantit);
	   return true;
	   }
	   return false;
   }
   
   /**
    * Modificarea datelor despre un produs din comanda
    * @param ID -id client
    * @param Denum d-denumire produs
    * @param cantit -cantitatea dorita
    * @return true daca s-a putut produce modificarea
    */
   public boolean modifItem(int ID,String Denum,int cantit)
   {
	   Customer q;
	   q=pers.find(ID, pers.rootP);
	   if(depozit.find(Denum, depozit.root)!=null)
	   {
		  return  q.actualizeaza(Denum, cantit);
	   }
	   return false;
   }
   
   
   /**
    * Calcaularea valorii unei comenzi
    * @param ID -id-ul clientului
    * @return -suma totata de plata
    */
   public float calcCom(int ID)
   
   {
	   float s=0;
	   Customer q;
	   String d;
	   q=pers.find(ID,pers.rootP);
	   if(q!=null)
	   	{
	    	for(int i=0;i<q.getNrProd();i++)
	    	{
	    		d=q.getDenum(i);
	    		s=s+depozit.getPret(d)*q.getCantItem(i);
	    			
	    	}
	    		
	   	}
	 return s;
	    
   }
   
   /**
    * Verifica daca in stoc sunt toate produsele comandate de un client
    * @param ID -cleintul care cere
    * @return true daca depozit contine toate cantitatile de produse dorite
    */
   public boolean verifStock(int ID)
   {
	   Customer q;
	   String d;
	   q=pers.find(ID,pers.rootP);
	   for(int i=0;i<q.getNrProd();i++)
	    {
	       d=q.getDenum(i);
	       int c=q.getCantItem(i);
	       if(c>depozit.getCantit(d))
	    	     return false;
	    			
	    }
	   return true;
   }
   
   /**
    * FInalizarea unui comenzi si intocmirea notei de plata
    * @param ID -id-ul clientului
    * @param nrComm -numarul comenzii
    * @param sel -modul de selectare a produselor 1: toate produsele , 2: toate produslele din stoc 3:tosate produsele cu stocul complet
    */
   public void terminareComanda(int ID,int nrComm,int sel)
   { String s=new String("Comanda"+nrComm+".txt");
	 PrintWriter fis;
	 Customer q;
	 String d;
	 float suma=0,pret;
	 int c;
	 q=pers.find(ID,pers.rootP);
	 try{ fis = new PrintWriter(new FileWriter(s));
	 	
	 		fis.println("                                                           Furnizor:  S.C. CumparaturiQ S.R.L.");
	 		fis.println("                                                           Adresa:    Cluj Napoca");
	 		fis.println("                                                           Telefon:   +0763620628");
	 		fis.println("                                                           Email:     somarariana@yahoo.com");
	 		fis.println();
	 		fis.println();
	 		fis.println("                       Factura NR. "+nrComm);
	 		fis.println();
	 		fis.println();
	 		Calendar cc = new GregorianCalendar();
	 		
	 		fis.println("Data facturarii: "+cc.get(Calendar.DAY_OF_MONTH) +"/"+(cc.get(Calendar.MONTH)+1)+"/"+cc.get(Calendar.YEAR));
	 		fis.println("Termen plata: 14 zile ");
	 		fis.println();
	 		fis.println();
	 		fis.println("CUMPARATOR\n ");
	 		String inputValue = JOptionPane.showInputDialog("Introduceti Numele/Prenuleme d-vostra"); 
            fis.println("Nume/Prenume:            "+inputValue);
            inputValue = JOptionPane.showInputDialog("Introduceti Adresa de facturare"); 
            fis.println("Adresa de facturare:     "+inputValue);
            inputValue = JOptionPane.showInputDialog("Introduceti numarul de telefon"); 
            fis.println("Telefon:                 "+inputValue);
            fis.println();
	 		fis.println();
	 		
	 		fis.println("Nr.crt.  Denumire    Cantitate Pret     Pret Total/Produs");
	 		fis.println();
	 		
	        for(int i=0;i<q.getNrProd();i++)
	         {d=new String("");
	        	switch(sel)
	        	{
	        	case -1: 
	        	    {
	        	    	 d=d+(i+1)+"        ";
	        	    	 d=d+q.getDenum(i)+"     ";
	        	    	 c=q.getCantItem(i);
	        	    	 d=d+c+"         ";
	        	    	 pret=depozit.getPret(q.getDenum(i));
	        	    	 d=d+pret+"      ";
	        	    	 pret=pret*c;
	        	    	 d=d+pret+"    ";
	        	    	 suma=suma+pret;
	        	         fis.println(d);
	        	    }
	        		break;
	        	case 0: //punem toate produsele cat avem in stoc
	        	 {
        	    	 d=d+(i+1)+"        ";
        	    	 d=d+q.getDenum(i)+"     ";
        	    	 c=q.getCantItem(i);
        	    	 
        	    	 if(c<=depozit.getCantit(q.getDenum(i)))
        	    	 {
        	    	    d=d+c+"         ";
        	    	    pret=depozit.getPret(q.getDenum(i));
        	    	    d=d+pret+"      ";
        	    	    pret=pret*c;
        	    	    d=d+pret+"    ";
        	    	    suma=suma+pret;
        	            fis.println(d);
        	         }
        	    	 else //punem doar cat avem in stoc
        	    	 {
        	    		c=depozit.getCantit(q.getDenum(i)) ;
        	    		d=d+c+"         ";
         	    	    pret=depozit.getPret(q.getDenum(i));
         	    	    d=d+pret+"      ";
         	    	    pret=pret*c;
         	    	    d=d+pret+"    ";
         	    	    suma=suma+pret;
         	            fis.println(d);
         	        }	 
        	    }
	        	 break;
	        	case 1://sarim peste produsele care nu au stock sufient
	        	 {
        	    	 d=d+(i+1)+"        ";
        	    	 d=d+q.getDenum(i)+"     ";
        	    	 c=q.getCantItem(i);
        	    	 
        	    	 if(c<=depozit.getCantit(q.getDenum(i)))
        	    	 {
        	    	    d=d+c+"         ";
        	    	    pret=depozit.getPret(q.getDenum(i));
        	    	    d=d+pret+"      ";
        	    	    pret=pret*c;
        	    	    d=d+pret+"    ";
        	    	    suma=suma+pret;
        	            fis.println(d);
        	         }
        	    }
	        		break;
	        	default: break;
	        	}
	             			
	         }
	     
	      fis.println("-------------------------------------------------------------------------------------------------------");  
	      fis.println("                                                     Suma totala de plata"+suma+"$");
	      suma=24*suma/124; //calculare tva
	      fis.println("                                                         din acre TVA(24%) : "+suma+"$");
		     	      
	      fis.println();
	 	  fis.println();
	      fis.println("Dupa expirarea termenului de plata la suna totala se aplica majorari de 1% pe saptamana!");
	      fis.println();
	 	  fis.println();
	      fis.println("                                                                Semnatura de primire:");
	      fis.println();
	 	  fis.println();
	      fis.println("                       Va multumim pentru alegerea facuta!");
	      
	   
		  fis.close();
		  
		  //modif cererea clientului si cantitatile din depozit
		  int i=0;
	      while(i<q.getNrProd())
	         {
	        	switch(sel)
	        	{
	        	case -1: 
	        	    {
	        	    	  c=q.getCantItem(i);
	        	    	  depozit.modifCantit(q.getDenum(i),depozit.getCantit(q.getDenum(i))- q.getCantItem(i));
	        	    	  q.removeItem(q.getDenum(i));
	        	    }
	        		break;
	        	case 0: //punem toate produsele cat avem in stoc
	        	 {
       	    	   	 c=q.getCantItem(i);
       	    	   	 
       	    	 if(c<=depozit.getCantit(q.getDenum(i)))
       	    	 {  
       	            depozit.modifCantit(q.getDenum(i),depozit.getCantit(q.getDenum(i))- q.getCantItem(i));
       	            q.removeItem(q.getDenum(i));
       	    	 }
       	    	 else //punem doar cat avem in stoc
       	    	 {
       	    		 
       	    		depozit.modifCantit(q.getDenum(i),0);
       	    		q.removeItem(q.getDenum(i));
       	    		//!!!!!!!!  se sterge din comanda pentru ca magazinul oricum nu mai dispune de acele produse
       	    		
       	    	 }	 
       	    }
	        	 break;
	        	case 1://sarim peste produsele care nu au stock sufient
	        	 {
       	    	 
       	    	 c=q.getCantItem(i);
       	    	 
       	    	 if(c<=depozit.getCantit(q.getDenum(i)))
       	    	 {
       	    	    
       	            depozit.modifCantit(q.getDenum(i),depozit.getCantit(q.getDenum(i))- q.getCantItem(i));
       	            q.removeItem(q.getDenum(i));
       	    	 }
       	    	 else i++; //sarim peste
       	       }
	        		break;
	        	default: break;
	        	}
	             			
	         }
       }
	 catch(Exception e){}
   }
   
}
